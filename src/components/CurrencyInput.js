import React, {Component} from 'react';

class CurrencyInput extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      amount: ''
    };
  }

  handleChange (event) {
    this.setState({
      amount: event.target.value
    })
  }

  render() {
    return(
      <section>
        <label>{this.props.currency}</label>
        <input
          type="text"
          onChange={this.handleChange}
          value={this.state.amount}
        />
      </section>
    )
  }
}

export default CurrencyInput;